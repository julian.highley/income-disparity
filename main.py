import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.image as mpimg
from PIL import Image

# Open Excel file
file_path = "/hdd/Documents/data/incometaxandbenefitdatabyincomedecileforallhouseholds.xlsx"
excel_file = pd.ExcelFile(file_path)

# Create dataframe from first tab
# df = pd.read_excel(excel_file, sheet_name=0, header=4)
df = pd.DataFrame()

# Loop through every tab and append data to dataframe
for sheet_name in excel_file.sheet_names[0:]:
    # Create new year column with sheet name
    year_col = pd.DataFrame({'Year': [sheet_name]})
    # Find row index that contains "Disposable income"
    for row_idx, row_data in pd.read_excel(excel_file, sheet_name=sheet_name, header=None, index_col=None).iterrows():
        if "Disposable income" in str(row_data.values):
            break
    else:
        print(f"Warning: 'Disposable income' not found in sheet {sheet_name}")
        continue
    # Extract data from selected row
    data = pd.read_excel(excel_file, sheet_name=sheet_name, header=None, skiprows=row_idx, nrows=1)
    # Combine year column and data
    year_data = pd.concat([year_col, data], axis=1)
    # Append to dataframe
    df = pd.concat([df, year_data], axis=0, ignore_index=True)

df.rename(columns={0: 'metric', 1: 'd1', 2: 'd2', 3: 'd3', 4: 'd4', 5: 'd5',
                   6: 'd6', 7: 'd7', 8: 'd8', 9: 'd9', 10: 'd10', 11: 'households'}, inplace=True)

df['disparity'] = df['d10'] / df['d1']
df['Year'] = df['Year'].astype(int)

# Create line chart
plt.plot(df['Year'], df['disparity'], color='white', lw=0.75)


# Add x and y axis labels
plt.xlabel("YEAR", fontsize=8)
plt.ylabel("INCOME DISPARITY", fontsize=8)
plt.title("INCOME INEQUALITY ROSE RAPIDLY in the EIGHTIES, BUT HAS STABALISED SINCE", pad=20, fontsize=12)

# Set y-axis limit to start at 0
plt.ylim(0, max(df['disparity']*1.2))

# Data for rectangles

political_party_in_power = [
    ("Labour", 1977, 1979, "images/james_callaghan.jpeg"),
    ("Conservative", 1979, 1990, "images/margaret_thatcher.jpeg"),
    ("Conservative", 1990, 1997, "images/john_major.jpeg"),
    ("Labour", 1997, 2007, "images/tony_blair.jpeg"),
    ("Labour", 2007, 2010, "images/gordon_brown.jpeg"),
    ("Conservative", 2010, 2016, "images/david_cameron.jpeg"),
    ("Conservative", 2016, 2019, "images/theresa_may.jpeg"),
    ("Conservative", 2019, 2021, "images/boris_johnson.jpeg")
]

# Create rectangles with different colors for different political parties
for party in political_party_in_power:
    if party[0] == "Labour":
        plt.axvspan(party[1], party[2], facecolor='#d50000', alpha=0.8)
    else:
        plt.axvspan(party[1], party[2], facecolor='#0087dc', alpha=0.8)

for party in political_party_in_power:
    img = Image.open(party[3])

    # Convert to grayscale
    img_gray = img.convert('L')
    img_arr = np.array(img_gray)

    # img = mpimg.imread(party[3])
    plt.imshow(img_arr, extent=[(((party[1]+party[2])/2)-0.75), (((party[1]+party[2])/2)+0.75),
                            max(df['disparity']*1.0), max(df['disparity']*1.10)],
               zorder=3, cmap='gray', aspect='auto', alpha=0.8)

    # Add circles to indicate start and end of PM's tenure
    plt.plot(party[1], max(df['disparity']*1.05), 'o', color='#36454F', alpha=0.01)
    plt.plot(party[2], max(df['disparity']*1.05), 'o', color='#36454F', alpha=0.01)

# Add footnote
plt.figtext(0.01, 0.005, 'Source: Office for National Statistics', ha='left', fontsize=8)
plt.tick_params(axis='both', which='both', length=0)

# Show the plot
#plt.show()
plt.savefig('charts/income_disparity_stabalised.png', bbox_inches='tight')