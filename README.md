# Income Disparity
For close to 50 years, the Office for National Statistics in the UK have been publishing
income disparity statistics. 

This code uses that information to create a chart that shows how the income of the wealthiest
10% of society has grown in comparison to the poorest 10% in the UK.

Furthermore, we show two different charts that highlight how perceptions of income disparity can be 
influenced by the way the data is presented.

### Income inequality is rampant and is continuing to get ever worse

![My Image](charts/income_disparity.png "Income Disparity")

This image shows quite stark picture of how income disparity has grown in the UK over the 
last 50 years. Through successive governments, the income of the wealthiest 10% of society 
has grown at a much faster rate than the income of the poorest 10% of society. In fact, for 
considerable periods of the Blair and Brown governments, income disparity grew at a rate similar
to that of the second half of the Thatcher government!

This chart is using disposable income as its basis. This is the income that is left after tax
and national insurance contributions have been paid. This is the income that people have to 
spend on food, clothing, housing, education, etc.

### Or has it stabalised?

![My Image](charts/income_disparity_stabalised.png' "Income Disparity Has Stabalised")
